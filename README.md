# Non-Removable Storage Layer

A storage layer based on NAND or FRAM storage that is non-removable (as opposed to, say, removable microSD-based storage).